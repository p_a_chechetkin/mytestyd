package framework;

public enum TIMEOUT {
	TEN_SEC(10),
	FIVE_SEC(5),
	ONE_SEC(1);

	private final int time;

	TIMEOUT(int time) {
		this.time = time;
	}

	public int getTimeInSeconds() {
		return time;
	}
}
