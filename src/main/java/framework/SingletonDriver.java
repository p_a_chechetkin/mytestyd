package framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.logerator.StudyLogger;

public class SingletonDriver {
	private static SingletonDriver singletonDriver;
	private final WebDriver driver;

	private SingletonDriver() {
		WebDriverManager.chromedriver().setup();
		this.driver = new ChromeDriver();
	}

	private static SingletonDriver getInstanceOfDriverWrapper() {
		StudyLogger.getLogger().info("Driver created");
		if (singletonDriver == null) {
			singletonDriver = new SingletonDriver();
		}
		return singletonDriver;
	}

	public static WebDriver getWebDriver() {
		return getInstanceOfDriverWrapper().driver;
	}
}
