package framework.elements;

import org.openqa.selenium.By;

public class Button extends HtmlElement {

	public Button(By locator) {
		super(locator);
	}

	public Button(HtmlElement parentElement, By locator) {
		super(parentElement, locator);
	}

	public void click() {
		getWebElement().click();
	}
}
