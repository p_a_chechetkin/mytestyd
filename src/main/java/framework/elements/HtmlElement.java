package framework.elements;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static framework.SingletonDriver.getWebDriver;
import static framework.TIMEOUT.ONE_SEC;
import static framework.TIMEOUT.TEN_SEC;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;
import static utils.logerator.StudyLogger.getLogger;

public class HtmlElement {
	private final By locator;
	private final WebDriver driver;
	private HtmlElement parentHtmlElement;
	private WebElement parentWebElement;

	public HtmlElement(By locator) {
		this.locator = locator;
		this.driver = getWebDriver();
	}

	public HtmlElement(HtmlElement parentElement, By locator) {
		this.locator = locator;
		this.parentHtmlElement = parentElement;
		this.driver = getWebDriver();
	}

	public HtmlElement(WebElement parentElement, By locator) {
		this.locator = locator;
		this.parentWebElement = parentElement;
		this.driver = getWebDriver();
	}

	public WebElement getWebElement() {
		if (parentWebElement != null) {
			return parentWebElement.findElement(locator);
		}
		if (parentHtmlElement != null) {
			parentHtmlElement.waitForElementVisible(ONE_SEC.getTimeInSeconds());
			return parentHtmlElement.getWebElement().findElement(locator);
		}
		waitForElementVisible(ONE_SEC.getTimeInSeconds());
		return driver.findElement(locator);
	}

	public void waitForElementVisible() {
		getLogger().info("Wait element: " + locator);
		waitForElementVisible(TEN_SEC.getTimeInSeconds());
	}

	public void waitElementNotDisplayed() {
		getLogger().info("Wait element is not displayed: " + locator);
		WebDriverWait wait = new WebDriverWait(driver, TEN_SEC.getTimeInSeconds());
		ExpectedCondition<Boolean> elementIsDisplayed = arg0 -> {
			try {
				getWebElement().isDisplayed();
				return false;
			} catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {
				return true;
			}
		};
		wait.until(elementIsDisplayed);
	}

	public void waitForElementVisible(int timeOutInSeconds) {
		getLogger().info("Wait element: " + locator + " Time: " + timeOutInSeconds);
		new WebDriverWait(driver, timeOutInSeconds)
				.until(visibilityOfElementLocated(locator));
	}

	public boolean isDisplayed() {
		try {
			waitForElementVisible(ONE_SEC.getTimeInSeconds());
		} catch (TimeoutException e) {
			return false;
		}
		return true;
	}

	public String getAttributeValue(String attribute) {
		return getWebElement().getAttribute(attribute);
	}

	public String getText() {
		return getWebElement().getText();
	}

	public void dragAndDropTo(WebElement elementTo) {
		Actions actions = new Actions(getWebDriver());
		WebElement source = this.getWebElement();
		actions.dragAndDrop(source, elementTo)
				.build()
				.perform();
	}
}
