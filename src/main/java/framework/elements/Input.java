package framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class Input extends HtmlElement {
	public Input(By locator) {
		super(locator);
	}

	public Input(HtmlElement parentElement, By locator) {
		super(parentElement, locator);
	}

	public void sendKeys(String login) {
		getWebElement().clear();
		getWebElement().sendKeys(login);
	}

	public void sendKeys(Keys keys) {
		getWebElement().clear();
		getWebElement().sendKeys(keys);
	}
}
