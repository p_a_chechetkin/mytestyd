package pages.yandex.mail;

import framework.elements.Button;
import framework.elements.HtmlElement;
import framework.elements.Input;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;
import resources.User;

import static framework.SingletonDriver.getWebDriver;
import static framework.TIMEOUT.TEN_SEC;
import static org.openqa.selenium.support.ui.ExpectedConditions.urlContains;

public class YandexMailLoginPage extends AbstractPage {
	private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
	private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
	private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//button[@type='submit']");
	private static final By ALERT_LOCATOR = By.xpath("//*[contains(@class, 'error-hint') or contains(@class, 'passport-Domik-Form-Error')]");

	//region Page steps
	public static void fillLoginInput(String login) {
		Input loginInput = new Input(LOGIN_INPUT_LOCATOR);
		loginInput.sendKeys(login);
	}

	public static void fillPasswordInput(String password) {
		Input passwordInput = new Input(PASSWORD_INPUT_LOCATOR);
		passwordInput.sendKeys(password);
	}

	public static void clickSubmitButton() {
		Button submitButton = new Button(LOGIN_BUTTON_LOCATOR);
		submitButton.click();
	}

	@Step("Perform login as {user.username}")
	public static void login(User user) {
		fillLoginInput(user.getUsername());
		clickSubmitButton();
		fillPasswordInput(user.getPassword());
		clickSubmitButton();
		try {
			new WebDriverWait(getWebDriver(), TEN_SEC.getTimeInSeconds())
					.until(urlContains("#inbox"));
		} catch (TimeoutException e) {
			throw new AssertionError(String.format("User %s was not login", user.getUsername()));
		}
	}
	//endregion


	public static boolean isAlertPresent() {
		return new HtmlElement(ALERT_LOCATOR).isDisplayed();
	}

	public static boolean isOpened() {
		Input loginInput = new Input(LOGIN_INPUT_LOCATOR);
		Input passwordInput = new Input(PASSWORD_INPUT_LOCATOR);
		return loginInput.isDisplayed() || passwordInput.isDisplayed();
	}
}
