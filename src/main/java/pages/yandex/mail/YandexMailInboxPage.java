package pages.yandex.mail;

import pages.yandex.mail.common.NavigationArea;
import pages.yandex.mail.common.YandexMailAbstractPage;
import pages.yandex.mail.common.emailsArea.Email;
import pages.yandex.mail.common.emailsArea.EmailsArea;

public class YandexMailInboxPage extends YandexMailAbstractPage {

	public static Email getLastEmailBySubject(String mailSubject) {
		return EmailsArea.getEmailsBySubject(mailSubject).get(0);
	}

	public static boolean isOpened() {
		return getNavigationBar().getActivePoint().equals(NavigationArea.NavigationPoint.INBOX_PAGE_POINT);
	}

}
