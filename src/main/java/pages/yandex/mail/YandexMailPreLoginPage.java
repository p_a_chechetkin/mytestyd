package pages.yandex.mail;

import framework.elements.Button;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.AbstractPage;
import utils.TestDataManager;

import static framework.SingletonDriver.getWebDriver;
import static utils.logerator.StudyLogger.getLogger;

public class YandexMailPreLoginPage extends AbstractPage {
	private static final By LOGIN_BUTTON_LOCATOR = By.className("HeadBanner-Button-Enter");

	@Step("Open yandex mail start page")
	public static void open() {
		String baseUrl = TestDataManager.getProperty("yandex_mail_start_page");
		getLogger().info("Open site: " + baseUrl);
		getWebDriver().get(baseUrl);
	}

	@Step("Go to login page from start page")
	public static void goToLoginPage() {
		getLogger().info("Go to login page");
		Button loginButton = new Button(LOGIN_BUTTON_LOCATOR);
		loginButton.click();
	}

	public static boolean isOpened() {
		return new Button(LOGIN_BUTTON_LOCATOR).isDisplayed();
	}
}