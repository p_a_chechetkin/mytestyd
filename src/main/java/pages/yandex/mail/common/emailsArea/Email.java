package pages.yandex.mail.common.emailsArea;

import framework.elements.Button;
import framework.elements.HtmlElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static framework.SingletonDriver.getWebDriver;
import static framework.TIMEOUT.FIVE_SEC;
import static framework.TIMEOUT.TEN_SEC;
import static org.openqa.selenium.support.ui.ExpectedConditions.attributeToBeNotEmpty;
import static org.openqa.selenium.support.ui.ExpectedConditions.frameToBeAvailableAndSwitchToIt;

public class Email {

	private static final By FILE_DISK_LINK_LOCATOR = By.xpath(".//a[@role='button']");
	private static final String ATTACHMENT_BY_NAME_LOCATOR_PATTERN = ".//span[@class='mail-File-Name' and text()='%s']/ancestor::span[contains(@class,'js-mail-file')]";
	private static final By EMAIL_SENDER_LOCATOR = By.className("mail-MessageSnippet-Item_sender");
	private static final By DISK_WIDGET_SAVE_IFRAME_LOCATOR = By.className("disk-widget-save");
	private static final By DOWNLOAD_TO_YDISK_BUTTON_LOCATOR = By.className("js-show-save-popup");
	private static final By EMAIL_SUBJECT_LOCATOR = By.className("mail-MessageSnippet-Item_subject");
	private final WebElement emailWebElement;
	private String sender;
	private String subject;

	Email(WebElement emailWebElement) {
		this.emailWebElement = emailWebElement;
	}

	public String getSender() {
		if (sender == null) {
			sender = emailWebElement.findElement(EMAIL_SENDER_LOCATOR).getText();
		}
		return sender;
	}

	public String getSubject() {
		if (subject == null) {
			subject = emailWebElement.findElement(EMAIL_SUBJECT_LOCATOR).getText();
		}
		return subject;
	}

	public Attachment getAttachmentByName(String fileName) {
		return new Attachment(this, fileName);
	}

	public static class Attachment {
		private final HtmlElement attachmentElement;
		private final String fileName;

		public Attachment(Email emailWebElement, String attachmentName) {
			this.fileName = attachmentName;
			By attachmentXpath = By.xpath(String.format(ATTACHMENT_BY_NAME_LOCATOR_PATTERN, attachmentName));
			this.attachmentElement = new HtmlElement(emailWebElement.emailWebElement, attachmentXpath);
			try {
				if (attachmentElement.isDisplayed())
					emailWebElement.emailWebElement.findElement(attachmentXpath);
			} catch (NoSuchElementException ignore) {
				throw new RuntimeException("This mail has not attachment with name " + attachmentName
						+ "\nMail subject:" + emailWebElement.getSubject());
			}
		}

		@Step("Download attachment file {this.fileName} to yandex disk")
		public void downloadFileOnYandexDisk() {
			Button downloadElementButton = new Button(
					attachmentElement,
					DOWNLOAD_TO_YDISK_BUTTON_LOCATOR);
			downloadElementButton.click();
			new WebDriverWait(getWebDriver(), FIVE_SEC.getTimeInSeconds())
					.until(frameToBeAvailableAndSwitchToIt(DISK_WIDGET_SAVE_IFRAME_LOCATOR));
			HtmlElement fileLinkElement = new HtmlElement(FILE_DISK_LINK_LOCATOR);
			fileLinkElement.waitForElementVisible();
			new WebDriverWait(getWebDriver(), TEN_SEC.getTimeInSeconds())
					.until(attributeToBeNotEmpty(
							fileLinkElement.getWebElement(), "href"));
			getWebDriver().switchTo().parentFrame();
		}

	}
}
