package pages.yandex.mail.common.emailsArea;

import framework.elements.HtmlElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.AbstractPage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static framework.SingletonDriver.getWebDriver;

public class EmailsArea extends AbstractPage {
	private static final String EMAIL_ITEM_LOCATOR_BY_SUBJECT_PATTERN = ".//span[@title='%s']/ancestor::div[@class='mail-MessageSnippet-Content']";

	public static List<Email> getEmailsBySubject(String mailSubject) {
		List<Email> emails = new ArrayList<>();
		By emailItemXpath = By.xpath(String.format(EMAIL_ITEM_LOCATOR_BY_SUBJECT_PATTERN, mailSubject));
		new HtmlElement(emailItemXpath).waitForElementVisible();
		List<WebElement> emailElements = getWebDriver().findElements(emailItemXpath);
		IntStream.range(0, emailElements.size())
				.forEach(index -> {
					WebElement emailItem = emailElements.get(index);
					emails.add(new Email(emailItem));
				});
		return emails;
	}
}
