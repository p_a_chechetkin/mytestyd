package pages.yandex.mail.common;

import framework.elements.Button;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.AbstractPage;
import pages.yandex.mail.common.emailsArea.EmailsArea;

import static framework.SingletonDriver.getWebDriver;

public abstract class YandexMailAbstractPage extends AbstractPage {
	private static final By USER_LOGO_LOCATOR = By.className("user-account");
	private static final By LOGOUT_BUTTON_LOCATOR = By.className("legouser__menu-item_action_exit");
	private static final By NEW_MAIL_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']");

	public static EmailsArea getEmailsArea() {
		return new EmailsArea();
	}

	public static NavigationArea getNavigationBar() {
		return new NavigationArea();
	}

	@Step("Open new mail pop up")
	public static void openNewMailPopUp() {
		Button newMailButton = new Button(NEW_MAIL_BUTTON_LOCATOR);
		if (!NewMailPopUp.isOpened()) {
			newMailButton.click();
		}
	}

	@Step("Log out from yandex mail")
	public static void logOut() {
		Button userLogoButton = new Button(USER_LOGO_LOCATOR);
		userLogoButton.click();
		Button logOutButton = new Button(LOGOUT_BUTTON_LOCATOR);
		logOutButton.waitForElementVisible();
		logOutButton.click();
		getWebDriver().manage().deleteAllCookies();
	}
}
