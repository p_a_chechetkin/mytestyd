package pages.yandex.mail.common;

import framework.elements.HtmlElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class SuccessfulSentPopUp extends YandexMailAbstractPage {
	private static final By HEADER_LOCATOR = By.className("ComposeDoneScreen-Title");

	@Step("Check is 'successful sent' message opened")
	public static boolean isOpened() {
		HtmlElement header = new HtmlElement(HEADER_LOCATOR);
		return header.isDisplayed();
	}
}