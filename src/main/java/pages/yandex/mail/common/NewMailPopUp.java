package pages.yandex.mail.common;

import framework.elements.Button;
import framework.elements.HtmlElement;
import framework.elements.Input;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.io.File;

import static utils.logerator.StudyLogger.getLogger;

public class NewMailPopUp extends YandexMailAbstractPage {

	private static final By SUBJECT_FIELD_LOCATOR = By.className("composeTextField");
	private static final By RECIPIENTS_FIELD_LOCATOR = By.cssSelector("div.tst-field-to div.MultipleAddressesDesktop-Field div");
	private static final By FILE_INPUT_LOCATOR = By.xpath("//div[@class='js-compose-footer-wrapper']//input[@type='file']");

	private static final By SEND_BUTTON_LOCATOR = By.xpath("(//div[@class='ComposeControlPanel-Part'][1]//button)[1]");
	private static final By ATTACH_BUBBLE_LOCATOR = By.xpath("(//div[@class='ComposeControlPanel-Part'][2]//button)[1]");
	private static final By ATTACH_INPUT_LOCATOR = By.xpath("//input[@class='ComposeAttachmentSourcesMenu-FileInput']");
	private static final By ATTACH_PROGRESS_PAR_LOCATOR = By.className("ComposeAttachmentsLoadingProgress");

	@Step("Set recipients for new mail")
	public static void setRecipients(String... recipients) {
		Input recipientField = new Input(RECIPIENTS_FIELD_LOCATOR);
		recipientField.waitForElementVisible();
		String recipientsText = String.join("\n", recipients) + "\n";
		getLogger().info("Write in field 'recipients': " + recipientsText);
		recipientField.sendKeys(recipientsText);
	}

	@Step("Set subject '{0}'")
	public static void setSubject(String subject) {
		Input subjectFiled = new Input(SUBJECT_FIELD_LOCATOR);
		getLogger().info("Write in field subject: " + subjectFiled);
		subjectFiled.sendKeys(subject);
	}

	@Step("Add files to email")
	public static void attachFilesFromPC(File... files) {
		new Button(ATTACH_BUBBLE_LOCATOR).click();
		for (File file : files) {
			String filePath = file.getAbsolutePath();
			getLogger().info("Attaching file : " + filePath);
			new Input(ATTACH_INPUT_LOCATOR).sendKeys(filePath);
		}
		new HtmlElement(ATTACH_PROGRESS_PAR_LOCATOR).waitElementNotDisplayed();
	}

	@Step("Send mail by clicking 'Send' button")
	public static void clickSendMailButton() {
		Button sendButton = new Button(SEND_BUTTON_LOCATOR);
		getLogger().info("Click send button");
		sendButton.click();
	}

	public static boolean isOpened() {
		return new Input(RECIPIENTS_FIELD_LOCATOR).isDisplayed();
	}

}