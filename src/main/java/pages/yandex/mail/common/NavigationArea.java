package pages.yandex.mail.common;


import framework.elements.Button;
import framework.elements.HtmlElement;
import org.openqa.selenium.By;
import pages.AbstractPage;

import java.util.Arrays;
import java.util.Optional;

public class NavigationArea extends AbstractPage {
	private static final By ACTIVE_POINT_LOCATOR = By.className("mail-NestedList-Item_current");

	public void navigateTo(NavigationPoint point) {
		Button pointButton = new Button(point.getNavigateOptionLocator());
		pointButton.click();
	}

	public NavigationPoint getActivePoint() {
		HtmlElement activePoint = new HtmlElement(ACTIVE_POINT_LOCATOR);
		String hrefOfActovePoint = activePoint.getAttributeValue("href");
		return NavigationPoint.getNavigationPointByHref(hrefOfActovePoint);
	}

	public enum NavigationPoint {
		INBOX_PAGE_POINT(By.xpath("//a[@href='#inbox' and @data-params='fid=1']"), "#inbox"),
		SENT_PAGE_POINT(By.xpath("//a[@href='#sent' and @data-params='fid=4']"), "#sent"),
		TRASH_PAGE_POINT(By.xpath("//a[@href='#trash' and @data-params='fid=3']"), "#trash"),
		DRAFT_PAGE_POINT(By.xpath("//a[@href='#draft' and @data-params='fid=6']"), "#draft"),
		SPAM_PAGE_POINT(By.xpath("//a[@href='#spam' and @data-params='fid=2']"), "#spam");

		private final By navigateOptionLocator;
		private final String href;

		NavigationPoint(By locator, String href) {
			this.navigateOptionLocator = locator;
			this.href = href;
		}

		public static NavigationPoint getNavigationPointByHref(String href) {
			Optional<NavigationPoint> first = Arrays.stream(values()).filter(it -> it.href.equals(href)).findFirst();
			if (first.isPresent()) {
				return first.get();
			}
			throw new RuntimeException("Not fount navigation point with this href attribute :" + href);
		}

		public String getHref() {
			return href;
		}

		public By getNavigateOptionLocator() {
			return navigateOptionLocator;
		}
	}
}
