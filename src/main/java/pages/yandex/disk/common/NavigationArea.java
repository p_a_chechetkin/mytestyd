package pages.yandex.disk.common;

import framework.elements.Button;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static framework.SingletonDriver.getWebDriver;
import static utils.TestDataManager.getWordByLanguage;

public class NavigationArea {
	private static final String FOLDER_ELEMENT_LOCATOR_PATTERN = "//a[@title='%s']";

	@Step("Navigate to {navigationPoint.folderName} folder")
	public void goToPage(NavigationPoint navigationPoint) {
		Button folderButton = new Button(navigationPoint.getLocator());
		folderButton.click();
	}

	public enum NavigationPoint {
		DOWNLOADS_PACKAGE("Downloads"),
		TRASH_PAGE("Trash");

		private final By locator;
		private final String folderName;

		NavigationPoint(String folderName) {
			this.folderName = folderName;
			this.locator = By.xpath(String.format(FOLDER_ELEMENT_LOCATOR_PATTERN, getWordByLanguage(folderName)));
		}

		public String getFolderName() {
			return folderName;
		}

		public By getLocator() {
			return locator;
		}

		public WebElement getWebElement() {
			return getWebDriver().findElement(locator);
		}

	}
}
