package pages.yandex.disk.common;

import framework.elements.HtmlElement;
import org.openqa.selenium.By;
import pages.AbstractPage;

public abstract class YandexDiskAbstractPage extends AbstractPage {
	private static final By PAGE_TITLE_LOCATOR = By.xpath("//h1");

	public static NavigationArea getNavigationArea() {
		return new NavigationArea();
	}

	public static FilesArea getFilesArea() {
		return new FilesArea();
	}

	protected static String getCurrentPageTitle() {
		return new HtmlElement(PAGE_TITLE_LOCATOR).getText();
	}
}