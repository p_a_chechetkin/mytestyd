package pages.yandex.disk.common;

import framework.elements.HtmlElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.AbstractPage;

import java.util.ArrayList;
import java.util.List;

public class FilesArea extends AbstractPage {
	private static final By FILES_AREA_LOCATOR = By.className("listing__items");
	private static final By FILE_ITEM_LOCATOR = By.className("listing-item_type_file");
	private static final By FILE_ICON_LOCATOR = By.className("listing-item__icon");
	private static final String FILE_ITEM_LOCATOR_BY_NAME_PATTERN = "//span[@title='%s']/../../..";
	private static final By ITEM_MOVED_NOTIFICATION_LOCATOR = By.className("notifications__item_moved");
	private final HtmlElement filesAreaElement = new HtmlElement(FILES_AREA_LOCATOR);

	public YandexDiskFileElement getFileByName(String filename) {
		return new YandexDiskFileElement(filesAreaElement.getWebElement()
				.findElement(By.xpath(String.format(FILE_ITEM_LOCATOR_BY_NAME_PATTERN, filename))));
	}

	public List<YandexDiskFileElement> getFiles() {
		List<WebElement> fileElements = filesAreaElement.getWebElement().findElements(FILE_ITEM_LOCATOR);
		List<YandexDiskFileElement> ydFiles = new ArrayList<>();
		fileElements.forEach(element -> ydFiles.add(new YandexDiskFileElement(element)));
		return ydFiles;
	}

	public class YandexDiskFileElement {
		private final WebElement element;
		private final String fileName;

		YandexDiskFileElement(WebElement element) {
			this.element = element;
			this.fileName = element.getText();
		}

		public WebElement getElement() {
			return this.element;
		}

		public WebElement getIconElement() {
			return this.element.findElement(FILE_ICON_LOCATOR);
		}

		public String getFileName() {
			return fileName;
		}

		@Step("Check is {this.fileName} displayed")
		public boolean isDisplayed() {
			return this.element.isDisplayed();
		}

		@Step("Drag and drop file {this.fileName} to {destinationElement}")
		public void dragAndDropTo(WebElement destinationElement) {
			HtmlElement fileIcon = new HtmlElement(element, FILE_ICON_LOCATOR);
			fileIcon.dragAndDropTo(destinationElement);
			HtmlElement successNotification = new HtmlElement(ITEM_MOVED_NOTIFICATION_LOCATOR);
			successNotification.waitForElementVisible();
		}
	}
}
