package pages.yandex.disk;

import framework.elements.HtmlElement;
import org.openqa.selenium.By;
import pages.yandex.disk.common.YandexDiskAbstractPage;
import utils.TestDataManager;

public class YandexDiskDownloadsPage extends YandexDiskAbstractPage {
	private static final By headerLocator = By.tagName("h1");

	public static boolean isOpened() {
		return new HtmlElement(headerLocator).getText().equals(TestDataManager.getWordByLanguage("Downloads"));
	}

}
