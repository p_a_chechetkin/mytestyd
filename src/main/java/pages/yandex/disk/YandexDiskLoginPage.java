package pages.yandex.disk;

import framework.elements.Button;
import framework.elements.Input;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.yandex.disk.common.YandexDiskAbstractPage;
import resources.User;

/**
 * Created by Pavel Chachotkin on 14.07.2017.
 */
public class YandexDiskLoginPage extends YandexDiskAbstractPage {
	private static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//button[@type='submit']");
	private static final By LOGIN_INPUT_LOCATOR = By.name("login");
	private static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

	public static boolean isOpened() {
		return new Input(LOGIN_INPUT_LOCATOR).isDisplayed() || new Input(PASSWORD_INPUT_LOCATOR).isDisplayed();
	}

	@Step("Login to yandex disk as {user.username}")
	public static void login(User user) {
		Input loginField = new Input(LOGIN_INPUT_LOCATOR);
		loginField.sendKeys(user.getUsername());
		Button submitLoginButton = new Button(SUBMIT_BUTTON_LOCATOR);
		submitLoginButton.click();

		Input passwordField = new Input(PASSWORD_INPUT_LOCATOR);
		passwordField.waitForElementVisible();
		passwordField.sendKeys(user.getPassword());
		Button submitPasswordButton = new Button(SUBMIT_BUTTON_LOCATOR);
		submitPasswordButton.click();
	}
}
