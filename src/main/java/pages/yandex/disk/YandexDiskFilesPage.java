package pages.yandex.disk;

import framework.elements.Button;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.yandex.disk.common.YandexDiskAbstractPage;
import utils.TestDataManager;

import static framework.TIMEOUT.FIVE_SEC;
import static utils.logerator.StudyLogger.getLogger;

public class YandexDiskFilesPage extends YandexDiskAbstractPage {
	private static final By CLOSE_WELCOME_MESSAGE_BUTTON_LOCATOR = By.xpath("//a[@class='_nb-popup-close js-dialog-close']");

	@Step("Close welcome message if it displayed")
	public static void closeWelcomeMessageIfPresent() {
		try {
			Button close = new Button(CLOSE_WELCOME_MESSAGE_BUTTON_LOCATOR);
			close.waitForElementVisible(FIVE_SEC.getTimeInSeconds());
			close.click();
			getLogger().info("window with start message closed");
		} catch (Exception e) {
			getLogger().info("window with start message was not faced");
		}
	}

	public static boolean isOpened() {
		return getCurrentPageTitle().equals(TestDataManager.getWordByLanguage("Files"));
	}

}
