package pages.yandex.disk;

import framework.elements.Button;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.AbstractPage;
import utils.TestDataManager;

import static framework.SingletonDriver.getWebDriver;
import static utils.logerator.StudyLogger.getLogger;

public class YandexDiskStartPage extends AbstractPage {

	private static final By LOGIN_BUTTON_LOCATOR = By.className("button_login");

	@Step("Open start page for yandex disk")
	public static void open() {
		String baseUrl = TestDataManager.getProperty("yandex_disk_start_page");
		getLogger().info("Open site: " + baseUrl);
		getWebDriver().get(baseUrl);
	}

	@Step("Go to login page for yandex disk")
	public static void goToLoginPage() {
		Button loginPageButton = new Button(LOGIN_BUTTON_LOCATOR);
		loginPageButton.waitForElementVisible();
		loginPageButton.click();
	}

	public static boolean isOpened() {
		return new Button(LOGIN_BUTTON_LOCATOR).isDisplayed();
	}

}
