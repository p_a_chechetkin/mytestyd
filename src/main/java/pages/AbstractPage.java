package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.WebDriverWait;

import static framework.SingletonDriver.getWebDriver;
import static framework.TIMEOUT.FIVE_SEC;

public abstract class AbstractPage {

	@Step("Refresh page")
	public static void refresh() {
		getWebDriver().navigate()
				.refresh();
	}

	public static void waitUntilOpened() {
		new WebDriverWait(getWebDriver(), FIVE_SEC.getTimeInSeconds())
				.until(driver -> isOpened());
	}

	public static boolean isOpened() {
		throw new RuntimeException("Method is opened not implemented yet");
	}
}
