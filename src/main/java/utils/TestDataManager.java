package utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import resources.User;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static utils.logerator.StudyLogger.getLogger;

public class TestDataManager {
	private static final List<User> users = new ArrayList<>();
	private static final String pathToUsersFile = "src/test/resources/users.json";
	private static final String pathToLanguagesFile = "src/main/resources/languages.json";
	private static final String PATH_TO_PROPERTIES_FILE = "src/test/resources/testEnvs.properties";
	private static final Properties props = new Properties();

	static {
		try {
			parseUsersFile();
			loadProperties();
		} catch (IOException e) {
			throw new RuntimeException("Can not read file", e);
		} catch (ParseException e) {
			throw new RuntimeException("Can not parse json file", e);
		}
	}

	private static void loadProperties() {
		try (InputStream in = new FileInputStream(PATH_TO_PROPERTIES_FILE)) {
			props.load(in);
		} catch (IOException e) {
			getLogger().error("Properties not loaded", e.getCause());
		}
	}

	private static void parseUsersFile() throws IOException, ParseException {
		JSONArray usersAsJSON = (JSONArray) new JSONParser().parse(new FileReader(pathToUsersFile));
		for (Object o : usersAsJSON) {
			JSONObject object = ((JSONObject) o);
			String id = object.get("id").toString();
			String userName = object.get("user_name").toString();
			String password = object.get("password").toString();
			String mail = object.get("email").toString();
			users.add(new User(id, userName, password, mail));
		}
	}

	public static String getWordByLanguage(String word) {
		String projectLanguage = props.get("project_language").toString();
		String result = null;
		try {
			JSONArray usersAsJSON = (JSONArray) new JSONParser().parse(new FileReader(pathToLanguagesFile));
			for (Object o : usersAsJSON) {
				JSONObject object = ((JSONObject) o);
				if (object.get("en").equals(word)) {
					result = (String) object.get(projectLanguage);
					break;
				}
			}
		} catch (IOException | ParseException e) {
			getLogger().error("Error when parse languages json file", e.getCause());
		}
		if (result == null) {
			throw new IllegalArgumentException("Have not translate for " + word + " to " + projectLanguage);
		} else {
			return result;
		}
	}

	public static User getUserById(String userId) {
		return users.stream()
				.filter(it -> it.getId().equals(userId))
				.findAny()
				.orElseThrow(() -> new RuntimeException("Can not find user with id : " + userId + "  in file " + pathToUsersFile));
	}

	public static String getProperty(String key) {
		return props.getProperty(key);
	}

}
