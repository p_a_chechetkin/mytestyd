package utils.logerator;

import org.apache.log4j.Logger;

public class StudyLogger {

	public static Logger getLogger() {
		StackTraceElement caller = new Throwable().fillInStackTrace().getStackTrace()[2];
		return Logger.getLogger(caller.getClassName() + "[" + caller.getMethodName() + "]");
	}

}
