package listeners;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import static framework.SingletonDriver.getWebDriver;
import static utils.logerator.StudyLogger.getLogger;

public class TestListener implements ITestListener {

	@Override
	public void onTestFailure(ITestResult result) {
		getLogger().error(String.format("Test %s finished. Status FAILED", result.getName()), result.getThrowable());
		takeScreenshot();
		takePageSource();
	}

	@Override
	public void onStart(ITestContext context) {
		getLogger().info("Run started");
	}

	@Override
	public void onTestStart(ITestResult result) {
		getLogger().info("Test started : " + result.getName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		getLogger().info("Test " + result.getName() + "finished. Status PASSED");
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		getLogger().info("Test " + result.getName() + " SKIPPED");
	}

	@Override
	public void onFinish(ITestContext context) {
		getLogger().info("Run finished");
	}

	@Attachment(value = "Page screenshot", type = "image/png")
	private byte[] takeScreenshot() {
		return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
	}

	@Attachment(value = "Page screenshot", type = "text/html", fileExtension = "html")
	private String takePageSource() {
		return getWebDriver().getPageSource();
	}
}
