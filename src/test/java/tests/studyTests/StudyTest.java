package tests.studyTests;

import io.qameta.allure.Issue;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.yandex.disk.*;
import pages.yandex.disk.common.FilesArea.YandexDiskFileElement;
import pages.yandex.mail.YandexMailInboxPage;
import pages.yandex.mail.YandexMailLoginPage;
import pages.yandex.mail.YandexMailPreLoginPage;
import pages.yandex.mail.common.NewMailPopUp;
import pages.yandex.mail.common.SuccessfulSentPopUp;
import pages.yandex.mail.common.emailsArea.Email;
import pages.yandex.mail.common.emailsArea.Email.Attachment;
import resources.User;
import tests.AbstractTest;
import util.FileCreator;
import utils.TestDataManager;

import java.io.File;

import static pages.yandex.disk.common.NavigationArea.NavigationPoint.DOWNLOADS_PACKAGE;
import static pages.yandex.disk.common.NavigationArea.NavigationPoint.TRASH_PAGE;

public class StudyTest extends AbstractTest {

	@Test(description = "check if a file saved from an email was successfully deleted")
	@Issue(value = "KNOWN ISSUE: Fail for step \"Open 'Downloads' folder in yandex disk\" for ENGLISH locale'")
	public void checkFileRemoving_0001() {
		File testAttachFile = FileCreator.createFileWithText();
		String mailSubject = "Test checkFileRemoving_0001";
		User user = TestDataManager.getUserById("user01");

		YandexMailPreLoginPage.open();
		YandexMailPreLoginPage.goToLoginPage();

		YandexMailLoginPage.login(user);

		YandexMailInboxPage.openNewMailPopUp();
		NewMailPopUp.setRecipients(user.getEmail());
		NewMailPopUp.setSubject(mailSubject);
		NewMailPopUp.attachFilesFromPC(testAttachFile);
		NewMailPopUp.clickSendMailButton();
		Assert.assertTrue(SuccessfulSentPopUp.isOpened(), "Successful pop up is not displayed");

		YandexMailInboxPage.refresh();
		Email mail = YandexMailInboxPage.getLastEmailBySubject(mailSubject);
		Attachment mailAttachment = mail.getAttachmentByName(testAttachFile.getName());
		mailAttachment.downloadFileOnYandexDisk();
		YandexMailInboxPage.logOut();

		YandexDiskStartPage.open();
		YandexDiskStartPage.goToLoginPage();

		YandexDiskLoginPage.login(user);
		YandexDiskFilesPage.closeWelcomeMessageIfPresent();
		YandexDiskFilesPage.getNavigationArea().goToPage(DOWNLOADS_PACKAGE);
		Assert.assertTrue(YandexDiskDownloadsPage.isOpened(), "Downloads page was not opened");

		YandexDiskFileElement yandexDiskFileElement = YandexDiskDownloadsPage.getFilesArea()
				.getFileByName(testAttachFile.getName());
		yandexDiskFileElement.dragAndDropTo(TRASH_PAGE.getWebElement());

		YandexDiskFilesPage.getNavigationArea().goToPage(TRASH_PAGE);

		yandexDiskFileElement = YandexDiskTrashPage.getFilesArea().getFileByName(testAttachFile.getName());

		Assert.assertTrue(yandexDiskFileElement.isDisplayed(),
				String.format("File %s not exist in trash folder", testAttachFile.getName()));
	}


}
