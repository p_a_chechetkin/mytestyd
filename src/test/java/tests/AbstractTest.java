package tests;

import framework.SingletonDriver;
import listeners.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;


@Listeners({TestListener.class})
public abstract class AbstractTest {
	protected WebDriver driver;

	@BeforeTest
	public void setup() {
		driver = SingletonDriver.getWebDriver();
		driver.manage().window().maximize();
	}

	@AfterTest
	public void cleanup() {
		driver.quit();
	}
}
