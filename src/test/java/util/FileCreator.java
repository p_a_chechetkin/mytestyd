package util;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static utils.logerator.StudyLogger.getLogger;

public class FileCreator {

	private static final int FILE_CONTENT_LENGTH = 20;
	private static final int FILE_ID_LENGTH = 5;

	public static File createFileWithText() {
		String fileContent = RandomStringUtils.randomAlphanumeric(FILE_CONTENT_LENGTH);
		String fileName = "TestFile_" + RandomStringUtils.randomNumeric(FILE_ID_LENGTH) + ".txt";
		File file = new File("src\\main\\resources\\" + fileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				getLogger().error("File " + file.getAbsolutePath() + " was not created", e.getCause());
			}
		}
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsoluteFile()))) {
			writer.write(fileContent);
			writer.close();
		} catch (IOException e) {
			getLogger().error("File " + file.getAbsolutePath() + " was not filled", e.getCause());
		}
		return file;
	}
}
