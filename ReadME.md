#Index

1. [Description](#Description)
1. [How to run tests](#How to run tests)
1. [How to generate allure report](#How to generate allure report)
1. [Required upcoming changes](#Required upcoming changes)

## Description

This study project is example of automation framework based on testNg engine.
I have created this project on automation courses in 2018.
I am updating this project and completing the [Required upcoming changes](#Required upcoming changes) list.

## How to run tests

1. Install maven ([Help link](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)) 
1. Set up your registered user for yandex applications in `src/test/resources/users.json` file 
1. Run next command in Command Prompt from project root folder:\
`mvn clean install`

## How to generate allure report

1. Install maven ([Help link](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)) 
1. Run tests
1. Run next command in Command Prompt from project root folder:\
`mvn serve:install`

[more info about allure 2](https://docs.qameta.io/allure/)

## Required upcoming changes 

- Add multi browsing
- Add supporting for multiple-users simulations
- Separate TestDataManager into TestDataManager and PropertiesManager
